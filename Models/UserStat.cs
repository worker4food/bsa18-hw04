
namespace Models
{
    public class UserStat
    {
        public User User { get; set; }
        public Post LastPost { get; set; }
        public int LastPostCommentsCount { get; set; }
        public Post MostPopularPost { get; set; }
        public Post MostLikedPost { get; set; }
        public int ActiveTasksCount {get; set; }
    }
}
