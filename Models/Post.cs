using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Models {
    public partial class Post
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("createdAt")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("body")]
        public string Body { get; set; }

        [JsonProperty("userId")]
        public long UserId { get; set; }

        [JsonProperty("likes")]
        public long Likes { get; set; }

        public IEnumerable<Comment> Comments { get; private set; }
        public User User { get; private set; }

        public Post withUser(User u) {
            User = u;
            return this;
        }

        public Post withComments(IEnumerable<Comment> cs) {
            Comments = cs;
            return this;
        }
    }
}
