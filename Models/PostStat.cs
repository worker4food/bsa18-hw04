
namespace Models
{
    public class PostStat
    {
        public Post Post { get; set; }
        public Comment LongestComment { get; set; }
        public Comment MostLikedComment { get; set; }
        public int BadOrShortCommentCount { get; set; }
    }

}
