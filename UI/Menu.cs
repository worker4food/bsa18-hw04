using System;
using Services;

namespace hw04
{
    public class MenuItem {
        public string text;
        public Action action;
    }

    public class Menu {
        private BlogData data;

        public void Start(BlogData bd) {
            data = bd;

            MenuItem[] mainMenu = {
            new MenuItem { text = "Show users posts and comments info (by user id)", action = UserPostsCommentsCount },
            new MenuItem { text = "Show users short (less than 50 symbols) comments (by user id) ", action = ShortUserComments },
            new MenuItem { text = "Show users completed todos (by user id)", action = CompletedUserTodos },
            new MenuItem { text = "Show sorted list of users with reverse sorted todos", action = AllUsersTodos },
            new MenuItem { text = "Show user statistcs (by user id)", action = UserStatistics },
            new MenuItem { text = "Show post statistics (by post id)", action = PostStatistics },
            new MenuItem { text = "Exit"}
        };
            Console.WriteLine();
            ShowMenu(mainMenu);
        }

        public void ShowMenu(MenuItem[] menu) {
            var theEnd = false;

            do {
                var i = 0;
                string msg = "";
                foreach(var item in menu) {
                    i++;
                    msg += String.Format("{0}. {1}\n", i, item.text);
                }

                Line();
                Console.WriteLine(msg);

                var act = GetInput(menu);

                if(act != null) {
                    try {
                        act();
                    }
                    catch(Exception e) {
                        Console.WriteLine(e.Message);
                    }
                }
                else
                    theEnd = true;

            } while (!theEnd);
        }

        protected void Line() => Console.WriteLine("====================");

        protected Action GetInput(MenuItem[] menu) {
            int index;

            while (true) {
                Console.Write("Enter your choice: ");
                if(int.TryParse(Console.ReadLine(), out index)) {
                    if(index > 0 && index <= menu.Length)
                        return menu[index - 1].action;
                }

            }
        }

        protected long GetId(string prompt = "Enter user ID: ") {
            while(true) {
                try {
                    Line();
                    Console.Write(prompt);
                    return long.Parse(Console.ReadLine());
                }
                catch(ArgumentException e) {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Try again");
                }
            }
        }

        private void UserPostsCommentsCount() {
            var id = GetId();
            Console.WriteLine("\nPost ID\t| Post text\t|Comments count");
            foreach(var r in data.UserPostsCommentsCount(id))
                Console.WriteLine($"{r.id}\t| {r.text}\t| {r.count}");
            Console.WriteLine();
        }

        private void ShortUserComments() {
            var id = GetId();
            Console.WriteLine("\nID\t| Comment text");
            foreach(var r in data.ShortUserComments(id))
                Console.WriteLine($"{r.id}\t| {r.text}");
            Console.WriteLine();
        }

        private void CompletedUserTodos() {
            var id = GetId();
            Console.WriteLine("\nID\t| Todo name");
            foreach(var r in data.CompletedUserTodos(id))
                Console.WriteLine($"{r.id}\t| {r.name}");
            Console.WriteLine();
        }

        private void AllUsersTodos() {
            Console.WriteLine("\nUser name\t| Todo name");
            foreach(var r in data.AllUsersTodos())
                Console.WriteLine($"{r.userName}\t| {r.todoName}");
            Console.WriteLine();
        }

        private void UserStatistics() {
            var r = data.UserStatistics(GetId());

            Console.WriteLine($"User name: {r.User.Name}");
            Console.WriteLine($"Last post: {r?.LastPost.Id} - {r?.LastPost.CreatedAt} - {r?.LastPost.Body}");
            Console.WriteLine($"Last post comments count: {r.LastPostCommentsCount}");
            Console.WriteLine($"Most popular post: {r?.MostPopularPost.Id} - {r?.MostPopularPost.CreatedAt} - {r?.MostPopularPost.Body}");
            Console.WriteLine($"Most liked post: {r?.MostLikedPost.Id} - {r?.MostLikedPost.CreatedAt} - {r?.MostLikedPost.Body}");
            Console.WriteLine($"Active tasks count: {r.ActiveTasksCount}\n");
        }

        private void PostStatistics() {
            var r = data.PostStatistics(GetId("Enter post ID: "));

            Console.WriteLine($"Post: {r.Post.CreatedAt} - {r.Post.Body}");
            Console.WriteLine($"Longest comment: {r?.LongestComment.Id} - {r?.LongestComment.Body}");
            Console.WriteLine($"Most liked comment: {r?.MostLikedComment.Id} - {r?.MostLikedComment.Body}");
            Console.WriteLine($"Sort or likeless comments count: {r.BadOrShortCommentCount}\n");
        }
    }
}
