﻿using System;
using Services;

namespace hw04
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var menu = new Menu();
            var data = BlogData.CreateInstanceAsync().Result;

            menu.Start(data);
        }
    }
}
